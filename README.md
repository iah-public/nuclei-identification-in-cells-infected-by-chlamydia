# Nuclei identification in cells infected by Chlamydia

A Fiji script for segmentation of nuclei in cells infected by Chlamydia.
