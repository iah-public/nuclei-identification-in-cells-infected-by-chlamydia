#@ File (label='Select a file') f
#@ File (label='Select a classifier') classifier
#@ Float (label='Probability threshold', min=0, max=1, value=0.75) percentage
#@ Float (label='Dynamic', min=1, value=3) dynamic

// Segmentation nuclei with weka + morpholibj + trackmate
// Author: Son Phan, IAH Pasteur
// Requirements:
//  - Trainable Weka segmentation (included in Fiji-TrackMate-Secret)
//  - TrackMate (included in Fiji-TrackMate-Secret)
//  - MorpholibJ: make sur the update sites: IJPB-plugins, ImageScience were ticked

print("\\Clear"); // Clear the log window
run("Close All"); // Close every open image
run("Clear Results"); // Clear result table
roiManager("reset"); // Empty the ROI manager

// Open Image
open(f);
basedir = File.getParent(f);
filename = File.getNameWithoutExtension(f);
rename('Raw');

// Select nuclei channel
run("Duplicate...", "duplicate channels=1-1");
rename('Nuclei');
waitForUser('Adjust the contrast of Nuclei (e.g. Auto > Apply) then click OK');;

// Trainable Weka
run("Trainable Weka Segmentation");
wait(3000);
selectWindow("Trainable Weka Segmentation v3.2.34");
call("trainableSegmentation.Weka_Segmentation.loadClassifier", toString(classifier));
call("trainableSegmentation.Weka_Segmentation.getProbability");

// Threshold the probability image
selectWindow("Probability maps");
run("Duplicate...", "duplicate range=1-1");
rename('Pb');
run("Duplicate...", " ");
run("8-bit");
run("Manual Threshold...", "min="+toString(round(percentage*255))+" max=255");
setOption("BlackBackground", true);
run("Convert to Mask");

// Morphological correction + watershed
run("Morphological Filters", "operation=Closing element=Disk radius=2");
run("Fill Holes (Binary/Gray)");
rename('Mask');
run("Distance Transform Watershed", "distances=[Borgefors (3,4)] output=[16 bits] normalize dynamic="+toString(dynamic)+" connectivity=4");
rename('Label');
run("glasbey on dark");

// Close some windows
close('Trainable Weka Segmentation*');
close('Probability maps');
close('Pb');
close('Pb-1');
close('Pb-1-Closing');
close('Mask');

// Merge channels
selectWindow("Raw");
run("Split Channels");
run("Merge Channels...", "c1=C1-Raw c2=C2-Raw c3=C3-Raw c4=Label create");
rename('Composite');

// TrackMate
run("TrackMate");








